class Mastermind
  def self.start(secret_combination)
    new(secret_combination)
  end

  def initialize(secret_combination)
    @secret_combination = secret_combination
  end

  def compare(guess_combination)
    comparation = { well_placed: 0, missplaced: 0 }

    guess_combination.each_with_index do |color, position|
      if well_placed?(color, position)
        comparation[:well_placed] += 1
      elsif missplaced?(color)
        comparation[:missplaced] += 1
      end
    end

    comparation
  end

  private

  def well_placed?(color, position)
    @secret_combination[position] == color
  end

  def missplaced?(color)
    @secret_combination.include?(color)
  end
end
