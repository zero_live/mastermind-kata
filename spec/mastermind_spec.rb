require 'mastermind'

describe 'Mastermind' do
  it 'does not give any pin when all colors are missed' do
    secret_combination = ['BLUE', 'BLUE', 'BLUE', 'BLUE']
    guess_combination = ['RED', 'RED', 'RED', 'RED']
    mastermind = Mastermind.start(secret_combination)

    comparation = mastermind.compare(guess_combination)

    all_colors_missed = { well_placed: 0, missplaced: 0 }
    expect(comparation).to eq(all_colors_missed)
  end

  it 'gives a missplaced pin when a color is present but not in place' do
    secret_combination = ['YELLOW', 'BLUE']
    guess_combination = ['BLUE', 'RED']
    mastermind = Mastermind.start(secret_combination)

    comparation = mastermind.compare(guess_combination)

    expect(comparation).to eq({ well_placed: 0, missplaced: 1 })
  end

  it 'gives a well placed pin when a color is present and in its place' do
    secret_combination = ['BLUE', 'YELLOW']
    guess_combination = ['BLUE', 'RED']
    mastermind = Mastermind.start(secret_combination)

    comparation = mastermind.compare(guess_combination)

    expect(comparation).to eq({ well_placed: 1, missplaced: 0 })
  end

  xit 'does not add a color as missplaced if that color exists as wellplaced' do
    secret_combination = ['BLUE', 'YELLOW', 'YELLOW']
    guess_combination = ['BLUE', 'BLUE', 'RED']
    mastermind = Mastermind.start(secret_combination)

    comparation = mastermind.compare(guess_combination)

    expect(comparation).to eq({ well_placed: 1, missplaced: 0 })
  end

  xit 'does not add a color as missplaced if that color exists as wellplaced' do
    secret_combination = ['YELLOW', 'BLUE', 'YELLOW']
    guess_combination = ['BLUE', 'BLUE', 'RED']
    mastermind = Mastermind.start(secret_combination)

    comparation = mastermind.compare(guess_combination)

    expect(comparation).to eq({ well_placed: 1, missplaced: 0 })
  end
end
